googleplus-feedflare
====================

Google+ FeedFlare For FeedBurner:

Simply follow the below instructions to add a Google+ share link to FeedBurner items.

1) Host the <a href="https://github.com/dbin78/googleplus-feedflare/blob/master/feedflare-googleplusshare.xml">XML file feedflare-googleplusshare.xml</a><br>
2) Go to <a href="http://feedburner.google.com/">FeedBurner</a> and login<br>
3) Select the feed you want to work with<br>
4) Go to the Optimize tab<br>
5) Click FeedFlare on the left side<br>
6) Scroll down to Personal Flare<br>
7) Add your Flare Unit URL (the path to the file hosted is step 1)<br>
8) Click the Add New Flare button<br>
9) Click the checkbox(es) next to your new unit<br>
10) Scroll down and press Save/Activate<br>

Additional resources:<br>
1) <a href="https://code.google.com/p/googleplus-feedflare">Google Code</a><br>
2) <a href="http://www.dainbinder.com/2013/03/how-to-add-google-sharing-feedflare-to.html">Blog Post</a> (Includes a hosted XML file for anyone to use.)

If you have any questions please let me know.
